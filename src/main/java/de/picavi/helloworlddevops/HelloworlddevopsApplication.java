package de.picavi.helloworlddevops;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HelloworlddevopsApplication {

	public static void main(String[] args) {
		SpringApplication.run(HelloworlddevopsApplication.class, args);
	}

}
